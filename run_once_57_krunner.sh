#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

kwriteconfig5 --file krunnerrc --group "Plugins" --key "CharacterRunnerEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "DictionaryEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "Kill RunnerEnabled" "true"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "PIM Contacts Search RunnerEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "PowerDevilEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "Spell CheckerEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "appmenurunnerEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "baloosearchEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "bookmarksEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "browsertabsEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "calculatorEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "desktopsessionsEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "jetbrainsrunnerEnabled" "true"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "katesessionsEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "kdevelopsessionsEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "konsoleprofilesEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "krunner_appstreamEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "kwinEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "locationsEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "org.kde.activitiesEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "org.kde.datetimeEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "org.kde.windowedwidgetsEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "placesEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "plasma-desktopEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "recentdocumentsEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "servicesEnable" "true"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "shellEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "unitconverterEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "webshortcutsEnabled" "false"
kwriteconfig5 --file krunnerrc --group "Plugins" --key "windowsEnabled" "false"
