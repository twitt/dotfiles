#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

kwriteconfig5 --file kded5rc --group "Module-appmenu" --key "autoload" "true"
kwriteconfig5 --file kded5rc --group "Module-baloosearchmodule" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-bluedevil" --key "autoload" "true"
kwriteconfig5 --file kded5rc --group "Module-browserintegrationreminder" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-colorcorrectlocationupdate" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-freespacenotifier" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-kded_accounts" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-keyboard" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-kscreen" --key "autoload" "true"
kwriteconfig5 --file kded5rc --group "Module-ksysguard" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-ktimezoned" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-networkmanagement" --key "autoload" "true"
kwriteconfig5 --file kded5rc --group "Module-networkstatus" --key "autoload" "true"
kwriteconfig5 --file kded5rc --group "Module-printmanager" --key "autoload" "true"
kwriteconfig5 --file kded5rc --group "Module-proxyscout" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-remotenotifier" --key "autoload" "false"
kwriteconfig5 --file kded5rc --group "Module-statusnotifierwatcher" --key "autoload" "true"
kwriteconfig5 --file kded5rc --group "Module-touchpad" --key "autoload" "true"
