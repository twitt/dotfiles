#!/bin/sh
which zsh >/dev/null 2>/dev/null || exit 0

zgen_path="$HOME/.local/share/zsh/zgen/"
if [ ! -d "$zgen_path" ]; then
  mkdir -p "$(dirname "$zgen_path")"
  git clone https://github.com/tarjoilija/zgen.git "${zgen_path}"
fi

true
