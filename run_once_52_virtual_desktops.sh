#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

count=$(qdbus org.kde.KWin /VirtualDesktopManager org.kde.KWin.VirtualDesktopManager.count)
declare -i count

[ "${count}" -ne 1 ] && exit 0
current=$(qdbus org.kde.KWin /VirtualDesktopManager org.kde.KWin.VirtualDesktopManager.current)
qdbus org.kde.KWin /VirtualDesktopManager org.kde.KWin.VirtualDesktopManager.setDesktopName "$current" "1"
qdbus org.kde.KWin /VirtualDesktopManager org.kde.KWin.VirtualDesktopManager.createDesktop 2 "2"
qdbus org.kde.KWin /VirtualDesktopManager org.kde.KWin.VirtualDesktopManager.createDesktop 3 "3"
qdbus org.kde.KWin /VirtualDesktopManager org.kde.KWin.VirtualDesktopManager.createDesktop 4 "4"
qdbus org.kde.KWin /KWin org.kde.KWin.setCurrentDesktop 1
