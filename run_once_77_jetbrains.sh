#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

if [ -x "$HOME/.local/bin/clion" ]; then
  if [ ! -f "$HOME/.local/share/applications/clion.desktop" ]; then
    cat >"$HOME/.local/share/applications/clion.desktop" <<EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=CLion
Icon=clion
Exec=$HOME/.local/bin/clion %f
Comment=A cross-platform IDE for C and C++
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-clion
EOF
    mkdir -p $HOME/.local/share/pixmaps
    cp $HOME/.local/share/JetBrains/Toolbox/apps/CLion/ch-0/*/bin/clion.svg $HOME/.local/share/pixmaps/
  fi
fi

if [ -x "$HOME/.local/bin/pycharm" ]; then
  if [ ! -f "$HOME/.local/share/applications/pycharm.desktop" ]; then
    cat >"$HOME/.local/share/applications/pycharm.desktop" <<EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=PyCharm
Icon=pycharm
Exec=$HOME/.local/bin/pycharm %f
Comment=Powerful Python IDE
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-pycharm
EOF
    mkdir -p $HOME/.local/share/pixmaps
    cp $HOME/.local/share/JetBrains/Toolbox/apps/PyCharm-P/ch-0/*/bin/pycharm.svg $HOME/.local/share/pixmaps/
  fi
fi

if [ -x "$HOME/.local/share/JetBrains/Toolbox/bin/jetbrains-toolbox" ]; then
  if [ ! -f "$HOME/.local/share/applications/jetbrains-toolbox.desktop" ]; then
    cat >"$HOME/.local/share/applications/jetbrains-toolbox.desktop" <<EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=JetBrains Toolbox
Icon=jetbrains
Exec=$HOME/.local/share/JetBrains/Toolbox/bin/jetbrains-toolbox
Comment=Toolbox
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-toolbox
EOF
    mkdir -p $HOME/.local/share/pixmaps
    cp $HOME/.local/share/JetBrains/Toolbox/toolbox.svg $HOME/.local/share/pixmaps/jetbrains.svg
  fi
fi
