#!/usr/bin/env sh
# vim: set ft=sh:

which vdirsyncer >/dev/null 2>/dev/null || exit 0

# see logs using journalctl --user -u vdirsyncer
vdirsyncer discover calendar
vdirsyncer discover contacts
systemctl --user enable vdirsyncer.timer
systemctl --user start vdirsyncer.timer
