#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

# https://github.com/UtkarshVerma/dotfiles/blob/master/extras/style.sh

# path for kde4
kde4_globals=$(realpath "${KDEHOME:-kde4}/share/config/kdeglobals")

# kwriteconfig5 is language specific, so switch to english for simplify things
export LANG=en

# set tool button style {{{
kwriteconfig5 --file "$kde4_globals" --group "Toolbar style" --key "ToolButtonStyle" "NoText"
kwriteconfig5 --file "$kde4_globals" --group "Toolbar style" --key "ToolButtonStyleOtherToolbars" "NoText"
kwriteconfig5 --file kdeglobals --group "Toolbar style" --key "ToolButtonStyle" "NoText"
kwriteconfig5 --file kdeglobals --group "Toolbar style" --key "ToolButtonStyleOtherToolbars" "NoText"
if [[ -f "$home/.config/gtk-3.0/settings.ini" ]]; then
  sed -i 's/^gtk-toolbar-style=.*/gtk-toolbar-style=0/' "$HOME/.config/gtk-3.0/settings.ini"
fi
if [[ -f "$home/.config/xsettingsd/xsettingsd.conf" ]]; then
  sed -i 's/^Gtk\/ToolbarStyle .*/Gtk\/ToolbarStyle 0/' "$HOME/.config/xsettingsd/xsettingsd.conf"
fi
if [[ -f "$home/.gtkrc-2.0" ]]; then
  sed -i 's/^gtk-toolbar-style=.*/gtk-toolbar-style=0/' "$HOME/.gtkrc-2.0"
fi
# }}}
# look and feel {{{
kwriteconfig5 --file kdeglobals --group "KDE" --key "LookAndFeelPackage" "org.kde.breezedark.desktop"
# }}}
# color schema {{{
# # create a new color scheme with darker selected background
cs_source="/usr/share/color-schemes/BreezeDark.colors"
if [ -f "$cs_source" ]; then
  cs_target="$HOME/.local/share/color-schemes/BreezeDarker.colors"
  cs_name="BreezeDarker"
  cs_name_de="BreezeDunkler"
  mkdir -p "$(dirname $cs_target)"
  cp "$cs_source" "$cs_target"
  kwriteconfig5 --file "$cs_target" --group "Colors:Selection" --key "BackgroundNormal" "39,114,152"
  kwriteconfig5 --file "$cs_target" --group "General" --key "ColorScheme" "$cs_name"
  kwriteconfig5 --file "$cs_target" --group "General" --key "Name" "$cs_name"
  # remove location specific stuff because it is not possible to set them using kwriteconfig5
  sed -i '/^Name\[/d' "$cs_target"
  kwriteconfig5 --file "$cs_target" --group "General" --key "NameDE" "dummy"
  sed -i "s/^NameDE.*$/Name\[de_DE\]=$cs_name_de/" "$cs_target"

  # sed -i '/^Name\[/d' "$HOME/.config/kdeglobals"
  # sed -i '/^Name\[/d' "$kde4_globals"

  # # insert into kdeglobals
  # ./qt_configurator.py "$cs_target" "$HOME/.config/kdeglobals"
  # ./qt_configurator.py "$cs_target" "$kde4_globals"
  #
  # # insert name for DE
  # kwriteconfig5 --file "$HOME/.config/kdeglobals" --group "General" --key "NameDE" "dummy"
  # kwriteconfig5 --file "$kde4_globals" --group "General" --key "NameDE" "dummy"
  # sed -i "s/^NameDE.*$/Name\[de_DE\]=$cs_name_de/" "$HOME/.config/kdeglobals"
  # sed -i "s/^NameDE.*$/Name\[de_DE\]=$cs_name_de/" "$kde4_globals"
  # kwriteconfig5 --file "$kde4_globals" --group "General" --key "ColorScheme" "$cs_name"
  # kwriteconfig5 --file "$kde4_globals" --group "General" --key "Name" "$cs_name"
  # kwriteconfig5 --file kdeglobals --group "General" --key "ColorScheme" "${cs_name// /}"
  # kwriteconfig5 --file kdeglobals --group "General" --key "Name" "$cs_name"
  # kwriteconfig5 --file kdeglobals --group "KDE" --key "ColorScheme" "$cs_name"
fi
# }}}
# non color scheme {{{
# kwriteconfig5 --file kdeglobals --group "General" --key "widgetStyle" "Breeze"
# kwriteconfig5 --file "$kde4_globals" --group "Icons" --key "Theme" "breeze-dark"
# kwriteconfig5 --file kdeglobals --group "Icons" --key "Theme" "breeze-dark"
# kwriteconfig5 --file kdeglobals --group "General" --key "widgetStyle" "Breeze"
# kwriteconfig5 --file kdeglobals --group "KDE" --key "widgetStyle" "Breeze"
# kwriteconfig5 --file kcminputrc --group "Mouse" --key "cursorTheme" "breeze_cursors"
# kwriteconfig5 --file plasmarc --group "Theme" --key "name" "breeze-dark"
# kwriteconfig5 --file kscreenlockerrc --group "Greeter" --key "Theme" "org.kde.breezedark.desktop"
# kwriteconfig5 --file ksplashrc --group "KSplash" --key "Theme" "org.kde.breeze.desktop"
# kwriteconfig5 --file ksplashrc --group "KSplash" --key "Engine" "KSplashQML"
# }}}
