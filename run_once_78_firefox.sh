#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

if [ -x "/usr/lib/firefox/firefox" ]; then
  if [ ! -f "$HOME/.local/share/applications/firefox_work.desktop" ]; then
    cat >"$HOME/.local/share/applications/firefox_work.desktop" <<EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=Firefox Work
GenericName=Web Browser for Work
Exec=/usr/lib/firefox/firefox -P Arbeit %u
Icon=firefox
Terminal=false
X-MultipleArgs=false
Type=Application
MimeType=text/html;text/xml;application/xhtml+xml;x-scheme-handler/http;x-scheme-handler/https;application/x-xpinstall;application/pdf;application/json;
Comment=Browse the World Wide Web
StartupNotify=true
StartupWMClass=firefox
Categories=Network;WebBrowser;
EOF
  fi
fi
