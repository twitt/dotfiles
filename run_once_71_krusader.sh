#!/usr/bin/env bash

which plasmashell >/dev/null 2>/dev/null || exit 0
kwriteconfig5 --file krusaderrc --group "Startup" --key "Show FN Keys" "false"
kwriteconfig5 --file krusaderrc --group "Startup" --key "Show actions tool bar" "false"
kwriteconfig5 --file krusaderrc --group "Startup" --key "Show job tool bar" "true"
kwriteconfig5 --file krusaderrc --group "Startup" --key "Show Cmd Line" "false"
kwriteconfig5 --file krusaderrc --group "Startup" --key "ToolBarsMovable" "Disabled"

kwriteconfig5 --file krusaderrc --group "PanelLayout" --key "FrameColor" "none"
kwriteconfig5 --file krusaderrc --group "PanelLayout" --key "FrameShadow" "Plain"
kwriteconfig5 --file krusaderrc --group "PanelLayout" --key "FrameShape" "NoFrame"
kwriteconfig5 --file krusaderrc --group "PanelLayout" --key "Layout" "krusader:compact"

kwriteconfig5 --file krusaderrc --group "Desktop Entry" --key "DefaultProfile" "zsh.profile"

kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Bookmarks Button Visible" "true"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Case Sensative Sort" "false"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "FlatOriginBar" "true"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Fullpath Tab Names" "false"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Fullscreen Terminal Emulator" "false"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "History Button Visible" "false"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Mouse Selection" "0"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Navigator Edit Mode" "false"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Navigator Full Path" "true"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Rename Selects Extension" "false"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Show Hidden" "true"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Show Tab Bar On Single Tab" "false"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Tab Bar Position" "top"
kwriteconfig5 --file krusaderrc --group "Look&Feel" --key "Single Instance Mode" "true"
