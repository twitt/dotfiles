" plugins {{{
" see https://vimawesome.com/
call plug#begin('~/.local/share/nvim/plugins')
" Aesthetics
Plug 'vim-airline/vim-airline' " nice info line
Plug 'vim-airline/vim-airline-themes' " theme
Plug 'Yggdroot/indentLine'
Plug 'junegunn/goyo.vim'  " distraction-free writing
Plug 'junegunn/limelight.vim' " focus tool
" Functionalities
Plug 'tpope/vim-fugitive' " adds git support
Plug 'lambdalisue/suda.vim' " use sudo when required
Plug 'tomtom/tcomment_vim' " simple commenting
Plug 'APZelos/blamer.nvim' " blame git
Plug 'preservim/nerdtree'
Plug 'airblade/vim-gitgutter' " shows git diff markers
Plug 'junegunn/vim-peekaboo' " shows registers
Plug 'w0rp/ale'
if filereadable("/usr/bin/node")
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  let g:coc_disable_startup_warning = 1
endif
" Append
Plug 'ryanoasis/vim-devicons' " icon support
" Plug 'terryma/vim-multiple-cursors' " adds multi cursers (Ctrl+N)
Plug 'mg979/vim-visual-multi', {'branch': 'master'}

call plug#end()
" }}}
" plugin preferences {{{
"   Aesthetics{{{
syntax enable
set background=dark
if $TERM =~ 'xterm-' || $TERM =~ 'konsole-'
  set mouse=r
endif
if $TERMFONT == 'nerdfont'
  let g:airline_powerline_fonts = 1
endif
if $COLORTERM == 'truecolor'
  set termguicolors
  let g:solarized_termtrans=0
  " support for tmux
  " let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  " let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
elseif $TERM =~ '-256color'
  set t_Co=256
  let g:solarized_termcolors=256
  let g:solarized_termtrans=0
else
  set t_Co=8
  let g:solarized_termtrans=0
  let g:solarized_termcolors=8
endif
colorscheme solarized8
" colorscheme solarized
let g:airline#extensions#tabline#enabled = 1
let g:airline_solarized_bg='dark'
let g:airline_theme='solarized'
" let g:airline_section_z = ' %{strftime("%-I:%M %p")}'
if exists("g:loaded_webdevicons")
  call webdevicons#refresh()
endif
" ┆ │ ▏
let g:indentLine_char = '▏'
" let g:indentLine_char_list = ['|', '¦', '┆', '┊']
" let g:indentLine_color_term = 239
" let g:indentLine_concealcursor = 'inc'
" let g:indentLine_conceallevel = 2
" let g:indentLine_setConceal = 0

let g:limelight_paragraph_span = 1
let g:limelight_default_coefficient = 0.7
" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

" Color name (:help gui-colors) or RGB color
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#777777'
let g:goyo_width = 120
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
"   }}}
"   diff {{{
if ! &diff
  let g:suda_smart_edit = 1
endif
" }}}
"   webdevicons {{{
set encoding=utf8
let g:webdevicons_enable = 1
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_airline_statusline = 1
let g:webdevicons_enable_nerdtree = 1
let g:WebDevIconsUnicodeGlyphDoubleWidth = 1
"   }}}
"   vim-gitgutter {{{
" You can jump between hunks with [c and ]c. You can preview, stage, and undo hunks with <leader>hp, <leader>hs, and <leader>hu respectively.
" toggle with :GitGutterToggle.
"   }}}
"   blamer {{{
"   :BlamerToggle
let g:blamer_prefix = ' > '
let g:blamer_template = '<committer-time> <committer> • <summary>'
let g:blamer_date_format = '%d.%m.%y'
"   }}}
" ale {{{
let g:airline#extensions#ale#enabled = 1
" let g:ale_completion_enabled = 1
set omnifunc=ale#completion#OmniFunc
let g:ale_enabled = 0
let g:ale_lint_on_text_changed = 'never'
" let g:ale_lint_on_insert_leave = 0
" let g:ale_lint_on_enter = 0
" let g:ale_lint_on_save = 1
let g:ale_set_signs = 1
let g:ale_sign_error = '' "
let g:ale_sign_warning = ''
let g:ale_sign_info = ''
" let g:ale_sign_column_always = 1
let g:ale_change_sign_column_color = 1
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
hi link ALEErrorLine ErrorMsg
hi link ALEWarningLine WarningMsg
hi ALEErrorSign     guifg=#dc322f guibg=#073642 guisp=NONE gui=NONE cterm=NONE
hi ALEWarningSign   guifg=#b58900 guibg=#073642 guisp=NONE gui=NONE cterm=NONE
hi ALEInfoSign      guifg=#268bd2 guibg=#073642 guisp=NONE gui=NONE cterm=NONE
hi ALESignColumnWithErrors guifg=#839496 guibg=#073642 guisp=NONE gui=NONE cterm=NONE
hi ALESignColumnWithoutErrors guifg=#839496 guibg=#073642 guisp=NONE gui=NONE cterm=NONE
" " }}}
" vim-chezmoi {{{

" let l:chezmoiBinary = 'chezmoi'
" }}}
" }}}
" configuration {{{
"   general {{{
filetype plugin on
set confirm            " dont warn about unsaved things, but ask
set noerrorbells       " don't beep
set lazyredraw         " don't update the display while executing macros
set showcmd
set backspace=indent,eol,start
set diffopt+=context:3,vertical
"   }}}
"   appearance {{{
set guifont=FiraCode\ Nerd\ Font\ 16
set number              " shows numers
set numberwidth=2
set relativenumber
set list                " show whitespaces
set listchars=tab:▸\ ,trail:…,precedes:<,extends:> " eol:¬,
set ignorecase    " ignore case when searching
set smartcase     " ignore case if search pattern is all lowercase
set incsearch hlsearch  " highlight search result
match ErrorMsg '^\(<\|=\|>\)\{7\}\([^=].\+\)\?$'
"   }}}
"   wrapping folding {{{
set ruler               " show 120 char ruler
set colorcolumn=120
" set wrap
set formatoptions=qrn1
set formatoptions+=1            " don't end lines single chars in paragraphs
" set foldlevelstart=1             " start out with everything folded
set textwidth=119
set foldenable                  " enable folding
set foldcolumn=0                " add a fold column
"   }}}
"   history and backup {{{
set history=100       " remember commands and search history
set undolevels=1000                       " use many muchos levels of undo
set noundofile                            " keep a persistent backup file
set backupskip=*private*,*/Privat/*
"   }}}
"   completion {{{
set isfname-==
set wildmenu                    " make tab completion for files/buffers act like bash
set wildmode=list:full          " show a list when pressing tab and complete  first full match
set wildignore+=.hg,.git,.svn " Version control
set wildignore+=*.aux,*.out,*.toc " LaTeX intermediate files
" set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg " binary images
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest " compiled object files
set wildignore+=*.spl " compiled spelling word lists
set wildignore+=*.sw? " Vim swap files
set wildignore+=*.pyc " Python byte code
"   }}}
"   tabs and indention {{{
filetype indent on                        " enables autmatic indentation as you type
set tabstop=4           " a tab is two spaces
set softtabstop=4   "Indent by 4 spaces when pressing <TAB>
set shiftwidth=4    "Indent by 4 spaces when using >>, <<, ==, autoindenting etc.
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'
set autoindent    " always set autoindenting on
"set smarttab      " insert tabs on the start of a line according to
                  "    shiftwidth, not tabstop
set smartindent     "Automatically inserts indentation in some cases
"   }}}
"   clipboard {{{
set clipboard=unnamed
"   }}}
"   spell correction {{{
setlocal spell spelllang=en
set spelllang=en,de
" set spellfile=~/.config/vim/de.add
     " set maximum number of suggestions listed to top 10 items
          " set sps=best,10
"   }}}
" }}}
" hotkeys {{{
" useful:
"   gx - open url
"   gf - open file
"   ZZ - save and quit
"   ZQ - quit
set hidden
let mapleader=","

"   helper functions {{{
function! ToggleMouse()
    " check if mouse is enabled
    if &mouse == 'a'
        " disable mouse
        set mouse=
    else
        " enable mouse everywhere
        set mouse=a
    endif
endfunc

function! AddEmptyLineBelow()
  call append(line("."), "")
endfunction

function! AddEmptyLineAbove()
  let l:scrolloffsave = &scrolloff
  " Avoid jerky scrolling with ^E at top of window
  set scrolloff=0
  call append(line(".") - 1, "")
  if winline() != winheight(0)
    silent normal! <C-e>
  end
  let &scrolloff = l:scrolloffsave
endfunction
if !exists("g:conceal_toggle")
       let g:conceal_toggle = 1
endif
function! ConcealToggle()
    if g:conceal_toggle == 0
        let g:conceal_toggle = 1
        set conceallevel=2
        echom "conceal on"
    else
        let g:conceal_toggle = 0
        set conceallevel=0
        echom "conceal off"
    endif
endfun
"   }}}

inoremap jk <Esc>
" Move normally between wrapped lines
nmap j gj
nmap k gk

" for command mode
nnoremap <S-Tab> <<
" for insert mode
inoremap <S-Tab> <C-d>

" F keys
nnoremap <silent><F1> <cmd>NERDTreeToggle<cr>
noremap <F2> :call ToggleMouse()<CR>
inoremap <F2> <Esc>:call ToggleMouse()<CR>a
nnoremap <F3> :Goyo<CR>
nnoremap <F4> :Limelight!!<CR>
xnoremap <F4> :Limelight!!<CR>
map <F5> :let &background = ( &background == "dark"? "light" : "dark" )<CR>
set pastetoggle=<F6>
nnoremap <F7> :setlocal spell!<CR>
inoremap <F7> <C-o>:setlocal spell!<CR>

" leader keys
nnoremap <leader>c :call ConcealToggle()<cr>
nnoremap <leader>n :let [&nu, &rnu] = [!&rnu, &nu+&rnu==1]<cr>
nnoremap <silent><leader>ev <C-w><C-v><C-l>:e $MYVIMRC<cr>
nnoremap <silent><leader>sv :so $MYVIMRC<CR>
nnoremap <leader>, :w<cr>
nnoremap <leader>i :IndentLinesToggle<cr>
nnoremap <leader><space> :noh<cr>
" search for word under the cursor
nnoremap <leader>/ "fyiw :/<c-r>f<cr>
" removing trailing whitespaces in current file
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>
nnoremap <leader>w :set wrap!<CR>
noremap <leader>y "+y
noremap <leader>p "+p
noremap <leader>Y "*y
noremap <leader>P "*p
vnoremap Y "+y
vnoremap P "+p
" replace
nnoremap <A-r> :%s#\<<C-r>=expand("<cword>")<CR>\>#

" Fugitive Conflict Resolution
nnoremap <leader>gd :Gvdiff<CR>
nnoremap gdh :diffget //2<CR>
nnoremap gdl :diffget //3<CR>
nmap <silent> <leader>gs :Gstatus<cr>
nmap <leader>ge :Gedit<cr>
nmap <silent><leader>gr :Gread<cr>
nmap <silent><leader>gb :Gblame<cr>

" nnoremap <C-w> <Esc>:q<CR>
nnoremap <Space><Space> za
vnoremap <Space><Space> za
nnoremap H 0
nnoremap L $

noremap <silent> <C-o> :call AddEmptyLineBelow()<CR>j
" noremap <silent> <> :call AddEmptyLineAbove()<CR>

" Select all text
" nmap <C-a> ggVG
" imap <C-p> <Esc>"*pa

" easier window change
" nnoremap <C-h> <C-w>h
" nnoremap <C-l> <C-w>l
" nnoremap <C-j> <C-w>j
" nnoremap <C-k> <C-w>k

nnoremap <leader>l :ALEToggle<cr>
nnoremap <C-k> :ALEPreviousWrap<cr>
nnoremap <C-j> :ALENextWrap<cr>
" " jump to tag
" nnoremap <C-CR> <C-]>
" " jump to tag but split vertically first
" nnoremap <S-CR> <C-w>v<C-]>
" " jump back
" nnoremap <C-BS> <C-T>
" " switch between current and last buffer
" nmap <leader>. <

" nmap <leader>j :set filetype=journal<CR>
" }}}
" file type specific {{{

au BufRead,BufNewFile *.desktop set iskeyword+=^=
au BufNewFile,BufRead ~/.local/share/applications/* setf desktop
au BufNewFile,BufRead ~/.config/*mimeapps.list    setf desktop
au BufNewFile,BufRead /etc/polkit-1/rules.d/* setf javascript | setl ts=4 sw=4 et
au BufNewFile,BufRead PKGBUILD* setf sh
au FileType yaml setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab autoindent
" zsh, vim, sh, conf {{{
au FileType zsh,vim,sh,conf setlocal foldmethod=marker
au FileType zsh,vim,sh,conf setlocal shiftwidth=4 tabstop=4 softtabstop=4 expandtab
" au FileType zsh,vim,sh,conf setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
" }}}
" Git {{{
au BufNewFile,BufRead */git/config setf gitconfig
au FileType gitconfig setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab autoindent
au BufNewFile,BufRead MERGE_MSG setf gitcommit
au FileType gitcommit setlocal tw=72 colorcolumn=72
au FileType gitcommit set spell spelllang=en_us
au FileType gitcommit match ErrorMsg '\%>72v.\+'
" }}}
" Groovy {{{
au FileType groovy setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab autoindent
" }}}
" json {{{
au FileType json setlocal conceallevel=0
let g:vim_json_syntax_conceal = 0
let g:vim_json_conceal = 0
" if this does not work, put this in a ftplugin file:
" setlocal conceallevel=0
" let g:vim_json_syntax_conceal = 0
" }}}
" cmake {{{
augroup filetype_cmake
  autocmd!
  autocmd filetype cmake setlocal shiftwidth=4 tabstop=4 softtabstop=4
  autocmd filetype cmake setlocal expandtab autoindent
  autocmd filetype cmake setlocal textwidth=120
  autocmd filetype cmake match ErrorMsg '\%>120v.\+'
augroup end
" }}}
" cpp {{{
augroup filetype_cpp
  autocmd!
  autocmd filetype cpp setlocal shiftwidth=4 tabstop=4 softtabstop=4
  autocmd filetype cpp setlocal expandtab autoindent
  autocmd filetype cpp setlocal textwidth=120
  autocmd filetype cpp match ErrorMsg '\%>120v.\+'
augroup end
" }}}
" }}}
" tweaks {{{
"   auto reload vim rc {{{
" Reloads vimrc after saving but keep cursor position
if !exists('*ReloadVimrc')
   fun! ReloadVimrc()
       let save_cursor = getcurpos()
       source $MYVIMRC
       call setpos('.', save_cursor)
   endfun
endif
autocmd! BufWritePost *nvim/init.vim call ReloadVimrc()
"   }}}
" Restore cursor position upon reopening files {{{
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

if has("folding")
  function! UnfoldCur()
    if !&foldenable
      return
    endif
    let cl = line(".")
    if cl <= 1
      return
    endif
    let cf  = foldlevel(cl)
    let uf  = foldlevel(cl - 1)
    let min = (cf > uf ? uf : cf)
    if min
      execute "normal!" min . "zo"
      return 1
    endif
  endfunction
endif
augroup resCur
  autocmd!
  if has("folding")
    autocmd BufWinEnter * if ResCur() | call UnfoldCur() | endif
  else
    autocmd BufWinEnter * call ResCur()
  endif
augroup END
"" }}}
" Cursorline {{{
" Only show cursorline in the current window and in normal mode.
augroup cline
    au!
    au WinLeave * set nocursorline
    au WinEnter * set cursorline
    au InsertEnter * set nocursorline
    au InsertLeave * set cursorline
augroup END
" }}}
" auto fix whitespaces {{{
let g:ale_cmake_cmakelint_options = '--linelength=120 --spaces=4'
let g:ale_cmake_cmakeformat_options = '-c /home/tobi/.config/clang-format --'

au BufNewFile,BufRead ~/* let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'sh': ['shfmt'],
\   'yaml': ['prettier'],
\}
let g:ale_fix_on_save = 1
" }}}
" }}}
