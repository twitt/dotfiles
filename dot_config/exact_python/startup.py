import atexit
import os
import readline
import sys

if (sys.version_info < (3, 0)):
     sys.exit(0)

if 'XDG_DATA_HOME' in os.environ:
    histdir = os.path.join(os.path.expanduser(os.environ['XDG_DATA_HOME']), 'python')
else:
    histdir = os.path.join(os.path.expanduser("~"), ".config", "python")
histfile = os.path.join(histdir, "python_history")

if not os.path.exists(histdir):
    os.makedirs(histdir, exist_ok=True)

try:
    readline.read_history_file(histfile)
    h_len = readline.get_current_history_length()
except FileNotFoundError:
    open(histfile, 'wb').close()
    h_len = 0

def save(prev_h_len, histfile):
    new_h_len = readline.get_current_history_length()
    readline.set_history_length(1000)
    readline.append_history_file(new_h_len - prev_h_len, histfile)
atexit.register(save, h_len, histfile)
