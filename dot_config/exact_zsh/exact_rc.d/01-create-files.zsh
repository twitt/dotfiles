[[ ! -d "${ZDATADIR}" ]] && mkdir -p "${ZDATADIR}"
[[ ! -d "${ZCACHEDIR}" ]] && mkdir -p "${ZCACHEDIR}"

fpath=($ZDOTDIR/completion $fpath)  # add custom completion scripts
