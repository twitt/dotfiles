# Correct commands.
setopt correct

# finder {{{
if [[ -x /usr/bin/fd ]]; then
  # debian/fedora: fd-find; arch: fd
  # usage: fd -e jpg -x convert {} {.}.png
  alias f='fd -H'
elif [[ -x /usr/bin/fdfind ]]; then
  alias f='fdfind -H'
else
  f() { local iname=$1; shift; echo "find . -iname \"*$iname*\"" "$@"; find . -iname "*$iname*" "$@" 2> /dev/null; }
fi
# }}}
# Disable correction. {{{
alias cd='nocorrect cd'
alias cp='nocorrect cp'
alias gcc='nocorrect gcc'
alias g++='nocorrect g++'
alias grep='nocorrect grep'
alias ln='nocorrect ln'
alias man='nocorrect man'
alias mkdir='nocorrect mkdir -p'
alias mv='nocorrect mv'
alias rm='nocorrect rm'
alias touch='nocorrect touch'
# }}}
# Disable globbing. {{{
alias find='noglob find'
test -x /usr/bin/scp && alias scp='noglob scp'
# }}}
# add color {{{
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias dmesg='dmesg --color=always'
export LESS=-R
alias ls='ls --color=auto'
# }}}
# ls {{{
if [[ -x /usr/bin/exa ]]; then
  alias ls='exa --group-directories-first'
  if [[ "${TERMFONT}" == "nerdfont" ]]; then
    alias ll='exa --group-directories-first --time-style long-iso --long --group --icons'
  else
    alias ll='exa --group-directories-first --time-style long-iso --long --group'
  fi
else
  alias ls='\ls -b -CF --color=auto --group-directories-first --human-readable --classify'
  alias ll='\ls -b -CF --color=auto --group-directories-first --human-readable --classify -l'
fi
alias lla='ll -a'
alias la='ls -a'
# }}}
# cd {{{
alias ..='cd ..'
alias ...='cd ../..'
alias ../.='cd ../..'
alias ./..='cd ../..'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../../'
alias cd.....='cd ../../../../'
# }}}
# adjustments and extensions {{{
test -x /usr/bin/ack && alias ack='nocorrect ack'
test -x /usr/bin/scp && alias scp='noglob scp'
test -x /usr/bin/bc && alias bc="bc -l -q"
test -x /usr/bin/ps && alias ps='ps auxf'
# test -x /usr/bin/systemctl && alias systemctl="sudo systemctl"
test -x /usr/bin/grep && alias grep="grep --color=auto --exclude-dir={.bzr,.cvs,.git,.hg,.svn}"
test -x /usr/bin/ag && alias ag='nocorrect ag --color --ignore-dir cmake-build-debug --ignore-dir cmake-build-release --ignore-dir build --skip-vcs-ignores'
test -x /usr/bin/powertop && alias powertop="sudo powertop"
# }}}
# dotfile adjustments {{{
test -x /usr/bin/svn && alias svn="/usr/bin/svn --config-dir ${HOME}/.config/subversion"
test -x /usr/bin/wget && alias wget='wget --hsts-file="${HOME}/.cache/wget-hsts"'
# test -x /usr/bin/bash && alias bash='bash --init-file $XDG_CONFIG_HOME/bash/bashrc'
# test -x /usr/bin/imagej && alias imagej='_JAVA_OPTIONS="-Xmx8192m -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Duser.home=$HOME/.local/share/imagej" imagej -p 1'
# should be supported now:
# test -x /usr/bin/mutt && alias mutt='command mutt -F "$XDG_CONFIG_HOME/mutt/muttrc"'
# test -x /usr/bin/xsel && alias xsel='/usr/bin/xsel --logfile $XDG_CACHE_HOME/xsel/xsel.log "$@" 2>/dev/null'
# test -x /usr/bin/tmux && alias tmux='test -d "$TMUX_TMPDIR" || command mkdir -p  "$TMUX_TMPDIR"; command tmux -f "${XDG_CONFIG_HOME}/tmux/tmux.conf"'
# }}}
# replacements {{{
# test -x /usr/bin/bat && alias cat=bat
if [[ -x /usr/bin/nvim ]]; then
  alias vim="nvim"
  alias ndiff="nvim -d"
  alias n="n"
elif [[ -x /usr/bin/vim ]]; then
  alias nvim="vim"
  alias ndiff="vimdiff"
  alias n="vim"
fi
test -x /usr/bin/trash-put && alias rm='trash-put'
if [[ -x /usr/bin/dfc ]] ; then
  alias df="dfc"
else
  test -x /usr/bin/df && alias df='df -h -T -l  | grep -v tmpfs'
fi
# }}}
# additional commands {{{
test -x /usr/bin/git && alias git-verbose="env GIT_CURL_VERBOSE=1 GIT_TRACE=1 GIT_SSH_COMMAND=\"ssh -vvv\" git"
if [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
  test -x /usr/bin/wl-copy && alias -g XC='| wl-copy'
else
  test -x /usr/bin/xclip && alias -g XC='| xclip -selection c'
fi
alias restart='sudo systemctl restart'
alias start='sudo systemctl start'
alias stop='sudo systemctl stop'
alias feierabend='sudo systemctl poweroff'
alias reboot="sudo systemctl reboot"
#alias rcz="source ${ZDOTDIR}/.zshrc"
# alias suspend="qdbus --system org.freedesktop.login1 /org/freedesktop/login1 org.freedesktop.login1.Manager.Suspend true"
# }}}
# chmod {{{
chmod-dirs() {
  if [ -z "$1" ]; then echo "please append a path"; return 1; fi;
  local mod; if [ -z "$2" ]; then mod=755; else mod=$2; fi;
  echo "find $1 -type d -exec chmod $mod {} \;"
  find "$1" -type d -exec chmod "$mod" {} \;
}

chmod-files() {
  if [ -z "$1" ]; then echo "please append a path"; return 1; fi;
  local mod; if [ -z "$2" ]; then mod=644; else mod=$2; fi;
  echo "find $1 -type f -exec chmod $mod {} \;"
  find "$1" -type f -exec chmod "$mod" {} \;
}
# }}}
# dusch {{{
dusch() {
  (( $# == 0 )) && set -- *

  if grep -q -i 'GNU' < <(du --version 2>&1); then
    du -khsc "$@" | sort -h -r
  else
    local size name
    local -a record

    while IFS=$'\n' read line; do
      record=(${(z)line})
      size="$(($record[1] / 1024.0))"
      name="$record[2,-1]"
      printf "%9.1LfM    %s\n" "$size" "$name"
    done < <(du -kcs "$@") | sort -n -r
    unset line
  fi
}
# }}}
# python {{{
pyclean () {
  find . -type f -name "*.py[co]" -delete
  find . -type d -name "__pycache__" -delete
}
# }}}
# git {{{
# No arguments: `git status`
# With arguments: acts like `git`
g() {
  if [[ $# -gt 0 ]]; then
    git $@
  else
    git status || true
  fi
}
compdef g=git
# }}}
# calc {{{
test -x /usr/bin/calc && alias c='calc -p --'
# if [[ -x /usr/bin/calc ]] ; then
# which calc > /dev/null && alias c='calc -p --'
# else
#   which calc_fallback > /dev/null && alias c='calc_fallback -p --'
# fi
if [[ -x /usr/bin/calc ]]; then
  function command_not_found_handler () {
    if [[ $1 == [0-9\-]* ]]; then
      calc -p -- $@
    else
      return 127
    fi
  }
fi
# }}}
