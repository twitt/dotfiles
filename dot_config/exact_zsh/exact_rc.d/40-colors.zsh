# {
autoload -U colors && colors
[[ -z "$TERM" ]] && print "use \"env TERM=xterm-256color /usr/bin/zsh\" to start zsh and have fun"
[[ "$COLORTERM" == (24bit|truecolor) || "${terminfo[colors]}" -eq '16777216' ]] || zmodload zsh/nearcolor 2> /dev/null
if [[ -x /usr/bin/dircolors ]]; then
  local f_
  [[ "$TERM" =~ ".*-256color.*" ]] && f_=dircolors.256dark || f_=dircolors.ansi-universal
  test -f "$ZDOTDIR/dircolors/$f_" && eval $(dircolors "$ZDOTDIR/dircolors/$f_")
fi
# }&!
