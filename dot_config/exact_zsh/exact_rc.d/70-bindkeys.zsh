# info {{{
#
# White key codes are easy to undertand, each of these 'normal' printing keys has six forms:
#
# A            = 'a'    (duhhh)
# A-Shift      = 'A'    (who would have guessed?)
# A-Alt        = '^[a'
#
# A-Ctrl       = '^A'
# A-Alt-Ctrl   = '^[^A'
# A-Alt-Shift  = '^[A'
# A-Ctrl-Shift = '^A'   (Shift has no effect)
#
# you need to patch linux.keytab:
#
# Pressing Backspace with a modifier (i.e. +AnyModifier) will send a special Escape Sequence. The star is in the binding definition will be replaced by the corresponding modifier: for example, pressing Shift+Backspace will send \E[9;2~, and Ctrl+Backspace will send \E[9;5~.
#   2 	Shift
#   3 	Alt
#   4 	Shift + Alt
#   5 	Control
#   6 	Shift + Control
#   7 	Alt + Control
#   8 	Shift + Alt + Control
#   9 and 10 seem to be free sequences
#
# remove:
#   Backspace      : "\x7f"
#   Return+Shift         : "\EOM"
#   Return-Shift+NewLine : "\r\n"
#   Return-Shift-NewLine : "\r"
# add:
#   Backspace+AnyModifier       : "\E[9;*~"
#   Backspace-AnyModifier       : "\x7f"
#   Return+AnyModifier          : "\E[10;*~"
#   Return+NewLine-AnyModifier  : "\r\n"
#   Return-NewLine-AnyModifier  : "\r"
#
# }}}
# exports {{{
# Return if requirements are not found.
[[ "$TERM" == 'dumb' ]] && return 1

# default value for $WORDCHARS
export DEF_WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>'
# take out the slash, period, angle brackets, dash and underscore here.
export WORDCHARS=${DEF_WORDCHARS//[\/.<>-_]}
export KEYTIMEOUT=10

# }}}
# Use human-friendly identifiers. {{{
zmodload zsh/terminfo
typeset -gA key_info
key_info=(
  'Control'      '\C-'
  # 'Escape'       '\e'
  'Meta'         '\M-'
  # 'Backspace'    "^?"
  'Home'         "$terminfo[khome]"
  'End'          "$terminfo[kend]"
  'Insert'       "$terminfo[kich1]"
  'Delete'       "$terminfo[kdch1]"
  'PageUp'       "$terminfo[kpp]"
  'PageDown'     "$terminfo[knp]"
  'Up'           "$terminfo[kcuu1]"
  'Left'         "$terminfo[kcub1]"
  'Down'         "$terminfo[kcud1]"
  'Right'        "$terminfo[kcuf1]"
  # shift tab
  'BackTab'      "$terminfo[kcbt]"
)

# Set empty $key_info values to an invalid UTF-8 sequence to induce silent
# bindkey failure.
for key in "${(k)key_info[@]}"; do
  if [[ -z "$key_info[$key]" ]]; then
    key_info[$key]='�'
  fi
done

function bind2maps () {
    local i sequence widget
    local -a maps

    while [[ "$1" != "--" ]]; do
        maps+=( "$1" )
        shift
    done
    shift

    if [[ "$1" == "-s" ]]; then
        shift
        sequence="$1"
    else
        sequence="${key_info[$1]}"
    fi
    widget="$2"

    [[ -z "$sequence" ]] && echo "skipped $1 ! $2" && return 1
    (( ${+widgets[${widget}]} )) || { echo "skipped $1 ! $2" && return 1 }

    for i in "${maps[@]}"; do
      bindkey -M "$i" "$sequence" "$widget"
    done
}

# }}}
# ZSH Base Config {{{
# e.g. required for updating vi input mode

# Exposes information about the Zsh Line Editor via the $editor_info associative
# array.
function editor-info {
  # Clean up previous $editor_info.
  unset editor_info
  typeset -gA editor_info

  if [[ "$KEYMAP" == 'vicmd' ]]; then
    zstyle -s ':omz:module:editor:keymap' alternate 'REPLY'
    editor_info[keymap]="$REPLY"
  else
    zstyle -s ':omz:module:editor:keymap' primary 'REPLY'
    editor_info[keymap]="$REPLY"

    if [[ "$ZLE_STATE" == *overwrite* ]]; then
      zstyle -s ':omz:module:editor:keymap:primary' overwrite 'REPLY'
      editor_info[overwrite]="$REPLY"
    else
      zstyle -s ':omz:module:editor:keymap:primary' insert 'REPLY'
      editor_info[overwrite]="$REPLY"
    fi
  fi

  unset REPLY

  zle reset-prompt
  zle -R
}
zle -N editor-info

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
# Updates editor information when the keymap changes.
function zle-keymap-select zle-line-init zle-line-finish {
  zle editor-info
}
zle -N zle-keymap-select

function zle-line-init () {
  # The terminal must be in application mode when ZLE is active for $terminfo
  # values to be valid.
  if (( $+terminfo[smkx] )); then
    # Enable terminal application mode.
    echoti smkx
  fi

  # Update editor information.
  zle editor-info
}
zle -N zle-line-finish

function zle-line-finish () {
  # The terminal must be in application mode when ZLE is active for $terminfo
  # values to be valid.
  if (( $+terminfo[rmkx] )); then
    # Disable terminal application mode.
    echoti rmkx
  fi

  # Update editor information.
  zle editor-info
}
zle -N zle-line-init


# Toggles emacs overwrite mode and updates editor information.
function overwrite-mode {
  zle .overwrite-mode
  zle editor-info
}
zle -N overwrite-mode
# }}}
# functions to call {{{
# edit command line in editor {{{
autoload -Uz edit-command-line
zle -N edit-command-line
# }}}
# run command line as user root via sudo {{{

__sudo-replace-buffer() {
    local old=$1 new=$2 space=${2:+ }
    if [[ ${#LBUFFER} -le ${#old} ]]; then
        RBUFFER="${space}${BUFFER#$old }"
        LBUFFER="${new}"
    else
        LBUFFER="${new}${space}${LBUFFER#$old }"
    fi
}

sudo-command-line() {
    [[ -z $BUFFER ]] && LBUFFER="$(fc -ln -1)"

    # Save beginning space
    local WHITESPACE=""
    if [[ ${LBUFFER:0:1} = " " ]]; then
        WHITESPACE=" "
        LBUFFER="${LBUFFER:1}"
    fi

    # Get the first part of the typed command and check if it's an alias to $EDITOR
    # If so, locally change $EDITOR to the alias so that it matches below
    if [[ -n "$EDITOR" ]]; then
        local cmd="${${(Az)BUFFER}[1]}"
        if [[ "${aliases[$cmd]} " = (\$EDITOR|$EDITOR)\ * ]]; then
            local EDITOR="$cmd"
        fi
    fi

    if [[ -n $EDITOR && $BUFFER = $EDITOR\ * ]]; then
        __sudo-replace-buffer "$EDITOR" "sudoedit"
    elif [[ -n $EDITOR && $BUFFER = \$EDITOR\ * ]]; then
        __sudo-replace-buffer "\$EDITOR" "sudoedit"
    elif [[ $BUFFER = sudoedit\ * ]]; then
        __sudo-replace-buffer "sudoedit" "$EDITOR"
    elif [[ $BUFFER = sudo\ * ]]; then
        __sudo-replace-buffer "sudo" ""
    else
        LBUFFER="sudo $LBUFFER"
    fi

    # Preserve beginning space
    LBUFFER="${WHITESPACE}${LBUFFER}"
}

zle -N sudo-command-line
# }}}
# old {{{
# lsFolder() { zle reset-prompt; echo; ls -b -CF --color=auto --group-directories-first --human-readable --classify}
# zle -N                 lsFolder
# cdUndoKey() { popd > /dev/null; zle reset-prompt; echo; ls; echo }
# zle -N                 cdParentKey
# cdParentKey() { pushd .. > /dev/null; zle reset-prompt; echo; ls; echo }
# zle -N                 cdUndoKey
# }}}
# Create directory under cursor or the selected area {{{
function inplaceMkDirs () {
    # Press ctrl-xM to create the directory under the cursor or the selected area.
    # To select an area press ctrl-@ or ctrl-space and use the cursor.
    # Use case: you type "mv abc ~/testa/testb/testc/" and remember that the
    # directory does not exist yet -> press ctrl-XM and problem solved
    local PATHTOMKDIR
    if ((REGION_ACTIVE==1)); then
        local F=$MARK T=$CURSOR
        if [[ $F -gt $T ]]; then
            F=${CURSOR}
            T=${MARK}
        fi
        # get marked area from buffer and eliminate whitespace
        PATHTOMKDIR=${BUFFER[F+1,T]%%[[:space:]]##}
        PATHTOMKDIR=${PATHTOMKDIR##[[:space:]]##}
    else
        local bufwords iword
        bufwords=(${(z)LBUFFER})
        iword=${#bufwords}
        bufwords=(${(z)BUFFER})
        PATHTOMKDIR="${(Q)bufwords[iword]}"
    fi
    [[ -z "${PATHTOMKDIR}" ]] && return 1
    PATHTOMKDIR=${~PATHTOMKDIR}
    if [[ -e "${PATHTOMKDIR}" ]]; then
        zle -M " path already exists, doing nothing"
    else
        zle -M "$(mkdir -p -v "${PATHTOMKDIR}")"
        zle end-of-line
    fi
}

zle -N inplaceMkDirs
# }}}
# }}}

bindkey -d
bindkey -v

bind2maps emacs             -- Home     beginning-of-line
bind2maps       viins vicmd -- Home     vi-beginning-of-line
bind2maps emacs             -- End      end-of-line
bind2maps       viins vicmd -- End      vi-end-of-line
bind2maps emacs viins       -- Insert   overwrite-mode
bind2maps             vicmd -- Insert   vi-insert
bind2maps emacs             -- Delete   delete-char
bind2maps       viins vicmd -- Delete   vi-delete-char
bind2maps emacs viins vicmd -- Up       up-line-or-search
bind2maps emacs viins vicmd -- Down     down-line-or-search
bind2maps emacs             -- Left     backward-char
bind2maps       viins vicmd -- Left     vi-backward-char
bind2maps emacs             -- Right    forward-char
bind2maps       viins vicmd -- Right    vi-forward-char
bind2maps emacs viins vicmd -- PageUp   history-beginning-search-backward
bind2maps emacs viins vicmd -- PageDown history-beginning-search-forward
bind2maps emacs viins       -- BackTab  reverse-menu-complete

if [[ "$TERM" == 'linux' ]]; then
else
  # bindkey -M emacs "\e[9;2~" backward-kill-word  # Ctrl-Backspace
  # bindkey -M emacs "\e[9;7~" clear-screen # Alt+Ent
  # URxvt sequences:
  bind2maps emacs viins vicmd -- -s '\eOc' forward-word
  bind2maps emacs viins vicmd -- -s '\eOd' backward-word
  # These are for xterm:
  bind2maps emacs viins vicmd -- -s '\e[1;5C' forward-word
  bind2maps emacs viins vicmd -- -s '\e[1;5D' backward-word
  # Ctrl + Backspace
  bind2maps emacs viins vicmd -- -s '\e[9;5~' backward-delete-word
  # Alt + Backspace
  bind2maps emacs viins vicmd -- -s '\e[9;3~' clear-screen
  # Ctrl + Del
  bind2maps emacs viins vicmd -- -s '\e[3;5~' delete-word
fi

bind2maps emacs viins       -- -s "$key_info[Control]o" inplaceMkDirs
bind2maps emacs viins       -- -s "$key_info[Control]y" backward-kill-line
bind2maps       viins       -- -s 'jk' vi-cmd-mode
bind2maps       viins vicmd -- -s '^[h' vi-backward-char
bind2maps       viins vicmd -- -s '^h' vi-backward-word
bind2maps       viins vicmd -- -s '^[H' vi-beginning-of-line
bind2maps       viins vicmd -- -s '^[l' vi-forward-char
bind2maps       viins vicmd -- -s '^l' vi-forward-word
bind2maps       viins vicmd -- -s '^[L' vi-end-of-line
bind2maps       viins vicmd -- -s '^[j' down-line-or-search
bind2maps       viins vicmd -- -s '^[k' up-line-or-search
bind2maps emacs viins vicmd -- -s '^[S' sudo-command-line
bind2maps emacs viins vicmd -- -s '^S' push-line-or-edit
bind2maps       viins vicmd -- -s '^x^l' clear-screen
bind2maps emacs viins       -- -s '^x^x' magic-space
bind2maps emacs viins       -- -s '^x^v' edit-command-line


if (( $+widgets[history-incremental-pattern-search-backward] )); then
  bind2maps emacs viins vicmd -- -s "^r" history-incremental-pattern-search-backward
  bind2maps emacs viins vicmd -- -s "^s" history-incremental-pattern-search-forward
fi

if [[ -n ${(M)keymaps:#menuselect} ]] ; then
  bind2maps menuselect -- BackTab reverse-menu-complete
  bind2maps menuselect -- -s '+' accept-and-menu-complete
  bind2maps menuselect -- Insert accept-and-menu-complete
fi

unset key{,map,bindings}
