# Different History files for root and standard user
if ((!EUID)); then
  HISTFILE=${ZHISTFILE}_root
else
  HISTFILE=${ZHISTFILE}
fi

HISTSIZE=100000    # The maximum number of events to save in the internal history.
SAVEHIST=$HISTSIZE # The maximum number of events to save in the history file.

export HISTIGNORE="&:ls:[bf]g:exit:reset:kill:clear:cd:cd ..:cd..:..:...:cd ...:l:ll"

setopt bang_hist        # Treat the '!' character specially during expansion.
setopt extended_history # save each command's beginning timestamp and the duration to the history file
setopt append_history   # append history list to the history file
setopt share_history    # import new commands from the history file also in other zsh-session

setopt hist_verify            # Do not execute immediately upon history expansion.
setopt hist_ignore_all_dups   # remove older duplicates
setopt hist_expire_dups_first # Expire a duplicate event first when trimming history.
setopt hist_save_no_dups      # Do not write a duplicate event to the history file.
setopt hist_find_no_dups      # Do not display a previously found event.
setopt hist_ignore_space      # don't store commands prefixed with a space
setopt hist_no_functions      # don't store function definitions
setopt hist_no_store          # don't store history (fc -l) command
setopt hist_reduce_blanks
setopt no_beep # avoid "beep"ingsetopt inc_append_history        # Write to the history file immediately, not when the shell exits.
