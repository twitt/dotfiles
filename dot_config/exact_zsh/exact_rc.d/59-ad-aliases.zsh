test -d "${HD_CORE_PATH}/python/core/traum/" && alias traum="${HD_CORE_PATH}/python/core/traum/main.py"

if [[ -d "${HD_CORE_PATH}/python/core/trabi/" ]]; then
  alias martin="${HD_CORE_PATH}/python/core/code_checker.py --name martin"
  alias trabi="${HD_CORE_PATH}/python/core/trabi/main.py"
  autoload -U bashcompinit
  bashcompinit
  eval "$(register-python-argcomplete ${HD_CORE_PATH}/python/core/trabi/main.py)"
  if [[ -d "${HD_CORE_PATH}/python/core/trabix/" ]]; then
    alias trabix="${HD_CORE_PATH}/python/core/trabix/main.py"
    eval "$(register-python-argcomplete ${HD_CORE_PATH}/python/core/trabix/main.py)"
  fi
fi

if [[ -d "${AD_COMMON_BUILD_PATH}/python/common_build/trabi/" ]]; then
  alias ad="${AD_COMMON_BUILD_PATH}/python/common_build/trabi/main.py"
  autoload -U bashcompinit
  bashcompinit
  eval "$(register-python-argcomplete ${AD_COMMON_BUILD_PATH}/python/common_build/trabi/main.py)"
fi
