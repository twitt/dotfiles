setopt auto_cd            # perform the cd command to directory   "$HOME/dir" -> cd $HOME/dir
setopt AUTO_PUSHD           # Push the old directory onto the stack on cd.
setopt PUSHD_IGNORE_DUPS    # Do not store duplicates in the stack.
setopt PUSHD_SILENT         # Do not print the directory stack after pushd or popd.
setopt PUSHD_TO_HOME        # Push to home directory when no argument is given.
# setopt cd_able_vars       # "cd home/dir" -> "cd $home/dir"
# setopt MULTIOS       # allows "echo foo >file1 >file2"
setopt extended_glob      # *~(*.gz|*.bz|*.bz2|*.zip|*.Z) -> searches for word not in compressed files
unsetopt CLOBBER            # Do not overwrite existing files with > and >>.
                            # Use >! and >>! to bypass.

# Abbreviations for paths
# hash -d pkg=/home/packages/
hash -d zsh=$ZDOTDIR
hash -d trash=$XDG_DATA_HOME/Trash


DIRSTACKSIZE=${DIRSTACKSIZE:-30}
# dirstack handling
if [[ -f ${ZDIRSTACKFILE} ]] && [[ ${#dirstack[*]} -eq 0 ]] ; then
    dirstack=( ${(f)"$(< $ZDIRSTACKFILE)"} )
# "cd -" won't work after login by just setting $OLDPWD, so
    [[ -d $dirstack[1] ]] && cd $dirstack[1] && cd $OLDPWD
fi

chpwd() {
    if (( $DIRSTACKSIZE <= 0 )) || [[ -z $ZDIRSTACKFILE ]]; then return; fi
    local -ax my_stack
    my_stack=( ${PWD} ${dirstack} )
    builtin print -l ${(u)my_stack} >! ${ZDIRSTACKFILE}
}

showLastNDirs() {
  emulate -L zsh
  autoload -U colors
  local color=$fg_bold[blue]
  local -i m=$1
  local -i n=$[$1 + $2 -1]
  local MAXDIRS=$[`dirs -p | wc -l` - 1]
  if [ $m -gt $n ]; then return 1; fi
  if [ $m -lt 0 ]; then n=0; fi
  if [ $n -gt $MAXDIRS ]; then n=$MAXDIRS; fi
  for i in $(seq $m $n); do
    local dir=`dirs -p | head -n $[$i + 1] | tail -n 1`
    local num="${$(printf "%-4d " $[$i+1])/ /.}"
    #local num="${$(printf "%-4d " $[$i-$m+1])/ /.}"
    printf " %s  $color%s$reset_color\n" $num $dir
  done
}

showLastDirs() {  # jump between directories
  emulate -L zsh
  local MAXDIRS=$[`dirs -p | wc -l` - 1]
  local MAXSHOW=5
  local m=0
  local -i dir=-1
  while [ $m -lt $MAXDIRS ]; do
    showLastNDirs $m $MAXSHOW
    let m=m+$MAXSHOW
    read -r 'dir?Jump to directory (0=next): ' || return
    (( dir == 0 )) && continue
    local -i realdir=$dir-1 #+$m-$MAXSHOW
    if (( realdir < 0 || realdir > MAXDIRS )); then
      echo no such directory stack entry: $dir
      return 1
    fi
    cd ~$realdir
    return
  done
}

d() {
  re='^[0-9]+$'
  if [ -z "$1" ]; then
    showLastDirs;
  elif [[ $1 =~ $re ]] ; then
    cd ~$[$1-1]
  else
    [[ -x `which pydictcc` ]] && pydictcc $1 | less
  fi
}
