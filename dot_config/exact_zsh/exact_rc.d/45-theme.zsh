# chars and definitions {{{
CURRENT_BG='NONE'

TV_DIR_LENGTH=40

if [ "${TERMFONT}" == "nerdfont" ]; then
  local distro
  distro=$(grep '^ID=' /etc/os-release | cut -d'=' -f2 | tr -d '\"')
  if [[ "${distro}" == "arch" ]]; then distro='' # blue
  elif [[ "${distro}" == "centos" ]]; then distro='' # yellow
  elif [[ "${distro}" == "ubuntu" ]]; then distro='' # orange
  elif [[ "${distro}" == "debian" ]]; then distro='' # red
  elif [[ "${distro}" == "raspbian" ]]; then distro='' # red
  elif [[ "${distro}" == "fedora" ]]; then distro='' # blue
  elif [[ "${distro}" == "rhel" ]]; then distro='' # red
  elif [[ "${distro}" == "gentoo" ]]; then distro=''
  elif [[ "${distro}" == "mint" ]]; then distro=''
  elif [[ "${distro}" == "docker" ]]; then distro='' # blue
  elif [[ "${distro}" == "alpine" ]]; then distro='' # blue
  else distro=''
  fi

  #   ↵ ✔                      
  #  ● ✚ ⍟  ↓ ↑ ☿  → ✔ ✘   ↵ ⚡  ⚙ …
  #            ﰛ   ✚ ✘     
  TC_SEPARATOR=''
  TC_RSEPARATOR=''
  TC_JOBS='⚙'
  TC_ROOT=''
  TC_USER=''
  TC_HOST=''
  TC_REMOTE=''
  TC_LINUX="${distro}"
  TC_COFFEE=''
  TC_TIME=''
  TC_RET_OK='✔'
  TC_RET_ERROR=''
  TC_RET_CANCEL='ﰸ '
  TC_RET_CMD_MISSING=' '
  TC_INS='ﬦ'
  TC_CMD=''
  TC_DIR='  '
  TC_HOME=''
  TC_TAG=''
  TC_HDD=''
  TC_GIT=''
  TC_STAGED=''
  TC_UNSTAGED=''
  TC_CHANGED='±'
  TC_MERGE=''
  TC_REBASE=''
  TC_BISECT=''
  TC_ETC='…'
  TC_VENV=''
else
  local distro
  distro=$(grep '^ID=' /etc/os-release | cut -d'=' -f2 | tr -d '\"')
  if [[ "${distro}" == "arch" ]]; then distro='A' # blue
  elif [[ "${distro}" == "centos" ]]; then distro='C' # yellow
  elif [[ "${distro}" == "ubuntu" ]]; then distro='U' # orange
  elif [[ "${distro}" == "debian" ]]; then distro='D' # red
  elif [[ "${distro}" == "raspbian" ]]; then distro='R' # red
  elif [[ "${distro}" == "fedora" ]]; then distro='F' # blue
  elif [[ "${distro}" == "rhel" ]]; then distro='R' # red
  elif [[ "${distro}" == "gentoo" ]]; then distro='G'
  elif [[ "${distro}" == "mint" ]]; then distro='M'
  elif [[ "${distro}" == "docker" ]]; then distro='D' # blue
  elif [[ "${distro}" == "alpine" ]]; then distro='A' # blue
  else distro='L'
  fi

  TC_SEPARATOR=''
  TC_RSEPARATOR=''
  TC_JOBS='J'
  TC_ROOT='R'
  TC_USER=''
  TC_HOST='@'
  TC_REMOTE=''
  TC_LINUX="${distro}"
  TC_COFFEE=''
  TC_TIME=''
  TC_RET_OK=''
  TC_RET_ERROR='!'
  TC_RET_CANCEL='X'
  TC_RET_CMD_MISSING='?'
  TC_INS='INS'
  TC_CMD='CMD' # e795
  TC_DIR='/'
  TC_HOME='~'
  TC_TAG='~'
  TC_HDD=''
  TC_GIT='G'
  TC_STAGED='+'
  TC_UNSTAGED='o'
  TC_CHANGED='*'
  TC_MERGE='M'
  TC_REBASE='R'
  TC_BISECT='B'
  TC_ETC='..'
  TC_VENV='VENV'
fi
# }}}
# prompt helper {{{
# Begin a segment
# Takes two arguments, background and foreground. Both can be omitted,
# rendering default background/foreground.
prompt_segment() {
  local bg fg
  [[ -n $1 ]] && bg="%K{$1}" || bg="%k"
  [[ -n $2 ]] && fg="%F{$2}" || fg="%f"
  if [[ $CURRENT_BG != 'NONE' && $1 != $CURRENT_BG ]]; then
    echo -n " %{$bg%F{$CURRENT_BG}%}${TC_SEPARATOR}%{$fg%} "
  else
    echo -n "%{$bg%}%{$fg%} "
  fi
  CURRENT_BG=$1
  [[ -n $3 ]] && echo -n $3
}

prompt_print() {
  local bg fg
  [[ -n $1 ]] && bg="%K{$1}" || bg="%k"
  [[ -n $2 ]] && fg="%F{$2}" || fg="%f"
  if [[ $CURRENT_BG != 'NONE' && $1 != $CURRENT_BG ]]; then
    echo -n "%{$bg%F{$CURRENT_BG}%}%{$fg%} "
  fi
  CURRENT_BG=$1
  [[ -n $3 ]] && echo -n $3
  echo -n "%{%k%f%}"
}

# Begin an RPROMPT segment
# Takes two arguments, background and foreground. Both can be omitted,
# rendering default background/foreground.
rprompt_segment() {
  local bg fg
  [[ -n $1 ]] && bg="%K{$1}" || bg="%k"
  [[ -n $2 ]] && fg="%F{$2}" || fg="%f"
  local ch="${TC_RSEPARATOR}"
  [[ "$4" == "none" ]] && ch=""
  if [[ $CURRENT_BG != 'NONE' && $1 != $CURRENT_BG ]]; then
    echo -n " %{%K{$CURRENT_BG}%F{$1}%}${ch}%{$bg%}%{$fg%} "
  else
    echo -n "%F{$1}%{%K{default}%}${ch}%{$bg%}%{$fg%} "
  fi
  CURRENT_BG=$1
  [[ -n $3 ]] && echo -n $3
}

# End the prompt, closing any open segments
prompt_end() {
  if [[ -n $CURRENT_BG ]]; then
    echo -n " %{%k%F{$CURRENT_BG}%}${TC_SEPARATOR}"
  else
    echo -n "%{%k%}"
  fi
  echo -n "%{%f%}"
  CURRENT_BG=''
}
# }}}
# show colors {{{
prompt_colors() {
  prompt_print black black "-"
  prompt_print white white "-"
  prompt_print yellow yellow "-"
  prompt_print red red "-"
  prompt_print magenta magenta "-"
  prompt_print blue blue "-"
  prompt_print cyan cyan "-"
  prompt_print green green "-"
}
# }}}
# who am i {{{
prompt_whoami() {
  local rfg ufg
  case ${KEYMAP} in
    vicmd) rfg='yellow' ; ufg='yellow' ;;
    main|viins|*) rfg='red' ; ufg='blue' ;;
  esac
  local symbols=()
  symbols+="%(!.%{%F{${rfg}}%}${TC_ROOT}.%{%F{${ufg}}%}${TC_LINUX})%(1j. %{%F{cyan}%}${TC_JOBS}.)"

  if [[ -n "$SSH_CLIENT" ]] || [[ -n "$SSH_TTY" ]]; then
    symbols+=" %{%f%}${TC_USER} %n ${TC_REMOTE} %m"
  else
    [[ "$(print -P %n)" != "$DEFAULT_USER" ]] && symbols+=" %{%f%}${TC_USER} %n"
    [[ "$(print -P %m)" != "$DEFAULT_HOST" ]] && symbols+=" %{%f%}${TC_HOST} %m"
  fi

  [[ -n "$symbols" ]] && prompt_segment black default "$symbols" "ns"
}
# }}}
# current working directory {{{
function get_unique_folder() {
  local trunc_path directory test_dir
  local -a matching
  local -a paths
  local cur_path='/'
  paths=(${(s:/:)1})
  for directory in ${paths[@]}; do
    test_dir=''
    for (( i=0; i < ${#directory}; i++ )); do
      test_dir+="${directory:$i:1}"
      matching=("$cur_path"/"$test_dir"*/)
      if [[ ${#matching[@]} -eq 1 ]]; then
        break
      fi
    done
    trunc_path+="$test_dir/"
    cur_path+="$directory/"
  done
  echo "${trunc_path: : -1}"
}
prompt_dir() {
  local tp=$(print -P "%~")

  local prefix tag ptag prefix_exp trunc_path
  if [[ "${tp}" == "~" ]]; then
    prefix="${TC_HOME}"
  elif [[ "${tp}" == "/" ]]; then
    prefix="${TC_HDD}"
  elif [[ "${tp:0:2}" == "~/" ]]; then
    prefix="${TC_HOME}"
    prefix_exp="${HOME}"
    trunc_path="${tp:2}"
  elif [[ "${tp:0:1}" == "~" ]]; then
    tag=${${tp%%\/*}:1}
    if [[ "${tag}" != "${tp:1}" ]]; then
      prefix_exp="${$(hash -d -m ${tag})#*=}"
      trunc_path="/${tp#*\/}"
    fi
    if [[ -n "$(eval echo \$\{HASH_${tag}_tag\})" ]]; then
      prefix="$(eval echo \$\{HASH_${tag}_tag\})"
    else
      prefix="${TC_TAG}"
      ptag="${tag}"
    fi
  elif [[ "${tp:0:1}" == "/" ]]; then
    prefix="${TC_HDD}"
    trunc_path="${tp:1}"
  else
    trunc_path="${tp}"
  fi

  if (( ${#trunc_path} > ${TV_DIR_LENGTH} )); then
    local parentdir=$(realpath -L "${prefix_exp}/${trunc_path}/..")
    local basedir=$(basename "${prefix_exp}/${trunc_path}")
    local parent_uniq="$(get_unique_folder ${parentdir})"
    if [[ -n "${prefix_exp}" ]]; then
      local prefix_uniq="$(get_unique_folder ${prefix_exp})"
      trunc_path="${parent_uniq//${prefix_uniq}/}/${basedir}"
    else
      trunc_path="${parent_uniq}/${basedir}"
    fi
  fi

  [[ "$TERM" == "linux" ]] && FG=white || FG=black
  prompt_segment blue "${FG}" "${prefix} ${ptag}${trunc_path//\//$TC_DIR}"

  if [[ -n "$VIRTUAL_ENV" ]]; then
    venv="${${VIRTUAL_ENV##*/}%%-*}"
    if [[ "${venv}" == "${tag}" ]]; then
      venv="${TC_VENV}"
    fi
    prompt_segment cyan ${FG} "${venv} "
  fi
}
# }}}
# git {{{

# Truncates the second argument to the length of the first and adds an ellipsis
prompt_truncate() {
  if [[ ${#2} -ge ${1}+3 ]]; then
    printf "%.${1}s${TC_ETC}" "${2}"
  else
    printf "%s" "${2}"
  fi
}
# Checks if working tree is dirty
is_workspace_dirty() {
  test -n "$(command git status --porcelain --ignore-submodules --untracked-files=no 2> /dev/null | tail -n1)"
}

is_no_git_workspace() {
  test "$(git rev-parse --is-inside-work-tree 2> /dev/null)" != "true"
}

# Git: branch/detached head, dirty status
prompt_git() {
  local mode dotgit branch color
  local symbols=()
  is_no_git_workspace && return
  is_workspace_dirty && color=yellow || color=green

  vcs_info

  branch=$(command git symbolic-ref HEAD 2> /dev/null) || \
    branch=$(command git rev-parse --short HEAD 2> /dev/null) || return 0

  dotgit="$(git rev-parse --git-dir 2>/dev/null)"
  if [[ -e "${dotgit}/BISECT_LOG" ]]; then
    mode="${TC_BISECT}"
  elif [[ -e "${dotgit}/MERGE_HEAD" ]]; then
    mode="${TC_MERGE}"
  elif [[ -e "${dotgit}/rebase" || -e "${dotgit}/rebase-apply" || -e "${dotgit}/rebase-merge" ]]; then
    mode="${TC_REBASE}"
  fi

  symbols+="${TC_GIT}"
  [[ "$branch" != "refs/heads/master" ]] && [[ "$branch" != "refs/heads/main" ]] && symbols+=$(prompt_truncate 15 "${branch/refs\/heads\//}")
  (( ${#vcs_info_msg_0_} > 1 )) && symbols+="${${vcs_info_msg_0_## }%% }"
  [[ -n "${mode}" ]] && symbols+="${mode}"

  prompt_segment $color black "${symbols}"
}

# }}}
# vi keymap {{{
prompt_keymap() {
  case ${KEYMAP} in
    vicmd)
      prompt_print default white "${TC_CMD} "
    ;;
    main|viins|*)
      prompt_print default green "${TC_INS} "
    ;;
  esac
}

zle-keymap-select () {
  zle reset-prompt
  zle -R
}
# }}}
# return status {{{
prompt_return_status() {
  local RETVAL=$1
  local symbols=()
  if [[ $RETVAL -eq 0 ]]; then # symbols+="%{%F{green}%}${TC_RET_OK}"
  elif [[ $RETVAL -eq 1 ]]; then symbols+="%{%F{red}%}${TC_RET_ERROR}"
  elif [[ $RETVAL -eq 127 ]]; then symbols+="%{%F{red}%}${TC_RET_CMD_MISSING}"
  elif [[ $RETVAL -eq 130 ]]; then symbols+="%{%F{red}%}${TC_RET_CANCEL}"
  else symbols+="%{%F{red}%}${RETVAL} ${TC_RET_ERROR}"
  fi
  [[ $ZSH_PROMPT_TIMER_SHOW -gt 0 ]] && symbols+="%{%F{cyan}%}${TC_TIME} ${ZSH_PROMPT_TIMER_SHOW}s"
  [[ -n "$symbols" ]] && rprompt_segment default default "$symbols" "none"
}
# }}}
# timestamp {{{
# Timestamp: add a timestamp to prompt - real time clock, stops when command is executed
prompt_timestamp() {
  local time_string="%H:%M:%S"
  rprompt_segment default default "%D{$time_string}" "none"
}
# }}}
# Main prompt
build_prompt() {
  prompt_whoami
  prompt_dir
  prompt_git
  prompt_end
  # prompt_keymap
}

# Right prompt
build_rprompt() {
  local RETVAL=$?
  prompt_return_status $RETVAL
  # prompt_colors
  prompt_timestamp
}

PROMPT='%{%f%b%k%}$(build_prompt) '
RPROMPT='%{%f%b%k%}$(build_rprompt)'

function preexec() {
  ZSH_PROMPT_TIMER=${timer:-$SECONDS}
}

function precmd() {
  if [ $ZSH_PROMPT_TIMER ]; then
    ZSH_PROMPT_TIMER_SHOW=$(($SECONDS - $ZSH_PROMPT_TIMER))
    unset ZSH_PROMPT_TIMER
  else
    unset ZSH_PROMPT_TIMER_SHOW
  fi
}

# Needed for clock in prompt
# TMOUT=1
# TRAPALRM() {
#     if [[ $WIDGET != *"complete"* && $WIDGET != *"beginning-search" ]]; then;
#       zle && { zle reset-prompt; zle -R }
#     fi
# }

prompt_setup() {
  # http://zsh.sourceforge.net/Doc/Release/User-Contributions.html#Version-Control-Information
  setopt prompt_subst
  autoload -Uz vcs_info

  zstyle ':vcs_info:*' enable git # available: vcs_info_printsys
  zstyle ':vcs_info:*' disable-patterns "*/.git(|/*)"
  zstyle ':vcs_info:*' get-revision true # commit hash via %i
  zstyle ':vcs_info:git*' check-for-changes true # unstaged & staged via %c %u
  zstyle ':vcs_info:*' stagedstr "${TC_STAGED}"
  zstyle ':vcs_info:*' unstagedstr "${TC_UNSTAGED}"
  zstyle ':vcs_info:*' formats '%u %c'
  zstyle ':vcs_info:*' actionformats '%u %c'
}

prompt_setup "$@"
