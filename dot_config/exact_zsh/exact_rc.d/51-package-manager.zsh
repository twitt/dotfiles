# creates an alias and precedes the command with

# [Pacman](https://wiki.archlinux.org/index.php/Pacman_Tips)
# [Aura](https://wiki.archlinux.org/index.php/Aura)
# [Aura](https://github.com/aurapm/aura)

if [[ -x /usr/bin/pacman ]]; then
  if [[ -x /usr/bin/yay ]]; then
    alias upgrade='yay -Syu'
    alias pacs='yay -S'
    alias pacss='yay -Ss'
    alias pacsi='yay -Si'
    alias pacdot='find /etc -regextype posix-extended -regex ".+\.pac(new|save)" 2> /dev/null'
    compdef pacs=yay
    compdef pacss=yay
    compdef pacsi=yay
  elif [[ -x /usr/bin/pacaur ]]; then
    alias upgrade='pacaur -Syu'
    alias pacs='pacaur -S'
    alias pacss='pacaur -Ss'
    alias pacsi='pacaur -Si'
    alias pacsre='pacaur -S --rebuild'
    compdef pacs=pacaur
    compdef pacsi=pacaur
    compdef pacsre=pacaur
  elif [[ -x /usr/bin/aura ]]; then
    alias upgrade='sudo aura -Syu && sudo aura -Aku '
    alias pacs='sudo aura -S'
    alias pacss='aura -Ss'
    alias pacsi='aura -Si'
    alias paca='sudo aura -A' #-x
    alias pacas='aura -As'
    alias pacai='aura -Ai'
    compdef pacs=aura
    compdef pacsi=aura
  else
    alias upgrade='sudo pacman -Syu'
    alias pacs='sudo pacman -S'
    alias pacss='pacman -Ss'
    alias pacsi='pacman -Si'
  fi
  alias pacl='pacman -Qlq'
  alias pacrm='sudo pacman -Rsn' # Remove configs
  alias pacclear='sudo paccache -r -k 1 -c /var/cache/pacman/pkg'
elif [[ -x /usr/bin/apt ]]; then
  alias pacs='sudo apt install'
  alias pacsi='apt-cache show'
  alias pacss='apt-cache search'
  alias pacrm='sudo apt remove'
  alias upgrade='sudo apt update && sudo apt upgrade'
  alias pacl='dpkg-query -L'
  compdef pacs=apt
  compdef pacrm=apt
  compdef pacl=dpkg-query
fi

orphans() {
  if [[ ! -n $(pacman -Qdt) ]]; then
    echo no orphans to remove
  else
    sudo pacman -Rs $(pacman -Qdtq)
  fi
}
