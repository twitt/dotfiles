setopt BRACE_CCL          # Allow brace character class list expansion.
setopt RC_QUOTES          # Allow 'Henry''s Garage' instead of 'Henry'\''s Garage'.
unsetopt MAIL_WARNING     # Don't print a warning message if a mail file has been accessed.
setopt nonomatch          # try to avoid the 'zsh: no matches found...'
setopt hash_list_all      # whenever a command completion is attempted, make sure the entire command path is hashed first.
setopt nohup              # Don't send SIGHUP to background processes when the shell exits.
setopt noglobdots         # * shouldn't match dotfiles. ever.
setopt noshwordsplit      # use zsh style word splitting

# job control
setopt LONG_LIST_JOBS     # List jobs in the long format by default.
setopt AUTO_RESUME        # Try resuming existing job before creating new one.
setopt AUTO_CONTINUE      # Auto-continue stopped jobs issued 'disown'
setopt NOTIFY             # Report status of background jobs immediately.
unsetopt BG_NICE          # Don't autorun background jobs at a lower priority.
unsetopt HUP              # Don't automatically kill jobs on shell exit.
unsetopt CHECK_JOBS       # Don't report on jobs when the shell exits.
setopt RM_STAR_WAIT
export REPORTTIME=20 # Say how long a command took
setopt unset              # don't error out when unset parameters are used

# shell
# watch for everyone but me and report logins
watch=(notme)

# Use hard limits, except for a smaller stack and no core dumps
unlimit
limit stack 8192
limit -s

# setting some default values
NOCOR=${NOCOR:-0}
NOMENU=${NOMENU:-0}
NOPRECMD=${NOPRECMD:-0}

# autoload zsh modules when they are referenced
zmodload -i zsh/parameter 2>/dev/null || print "Notice: no parameter available :("
zmodload -i zsh/complist 2>/dev/null || print "Notice: no complist available :("
zmodload -i zsh/deltochar 2>/dev/null || print "Notice: no deltochar available :("
zmodload -i zsh/mathfunc 2>/dev/null || print "Notice: no mathfunc available :("
zmodload zsh/stat
zmodload zsh/zpty
zmodload zsh/mapfile
# autoloading
autoload zmv
autoload zed
