export ZGEN_CUSTOM_COMPDUMP=${ZCOMPDUMPFILE}
source_verbose $ZGEN_DIR/zgen.zsh
if ! zgen saved; then
  zgen load zsh-users/zsh-completions src
  zgen load zsh-users/zsh-history-substring-search
  zgen load zsh-users/zsh-autosuggestions
  zgen load zdharma/fast-syntax-highlighting
  zgen save
fi
unset ZGEN_CUSTOM_COMPDUMP
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#666666,bg=8"
# bindkey '^ ' autosuggest-accept
