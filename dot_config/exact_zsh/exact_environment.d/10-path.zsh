function export_append() {
  if [ -d "$2" ]; then
    eval export $1="$2:\$$1"
  fi
}

# Bin Paths
path=( /usr/local/{bin,sbin} /usr/{bin,sbin} $path )

export_append PYTHONPATH "${HOME}/.local/python"
export_append LD_LIBRARY_PATH "${HOME}/.local/lib"
export_append PATH "${HOME}/.local/bin"
export_append PATH "${HOME}/.local/bin/public"
export_append XDG_DATA_DIRS "/var/lib/flatpak/exports/share"
export_append XDG_DATA_DIRS "${XDG_DATA_HOME}/flatpak/exports/share"
unset -f export_append

typeset -gx path

# locale setup
for var in LANG LC_ALL LC_MESSAGES ; do
    [[ -n ${(P)var} ]] && export $var
done

# automatically remove duplicates from these arrays
typeset -Ugx path cdpath fpath manpath

# }}}
