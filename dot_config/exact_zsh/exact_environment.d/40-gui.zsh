# export SSH_ASKPASS=/usr/bin/ksshaskpass
# export QT_PLUGIN_PATH=$XDG_DATA_HOME/kate/plugins:$QT_PLUGIN_PATH
# export GTK_MODULES=canberra-gtk-module
export GTK_MODULES=appmenu-gtk-module

export DOTNET_CLI_TELEMETRY_OPTOUT=1

# use same settings in GTK as in Qt
export FREETYPE_PROPERTIES="cff:no-stem-darkening=0"

# libre office
export VCL_ICONS_FOR_DARK_THEME=1
# export SAL_USE_VCLPLUGIN=kf5
# needed for chromium
export KDE_FULL_SESSION=true
export KDE_SESSION_VERSION=5

if [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
  # QT_QPA_PLATFORM=wayland-egl
  export QT_QPA_PLATFORM=wayland
  export CLUTTER_BACKEND=wayland
  export SDL_VIDEODRIVER=wayland
  export GDK_BACKEND=wayland
  # firefox
  export MOZ_ENABLE_WAYLAND=1
fi

# QT_AUTO_SCREEN_SCALE_FACTOR=0
# QT_SCALE_FACTOR=0.8
# GDK_SCALE=0.8
