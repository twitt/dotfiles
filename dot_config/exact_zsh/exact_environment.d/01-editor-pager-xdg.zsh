export EDITOR=/usr/bin/nvim
export SUDO_EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/kate
export BROWSER=/usr/bin/firefox
export PAGER=/usr/bin/less
export MANPAGER="/bin/sh -c \"col -b | nvim -c 'set ft=man ts=8 nomod nolist nonu noma' -\""
#export DIFFTOOL=meld

# for further information see
# https://wiki.archlinux.org/index.php/XDG_Base_Directory
# https://specifications.freedesktop.org/basedir-spec/latest/ar01s02.html
export XDG_DATA_HOME=${XDG_DATA_HOME:-${HOME}/.local/share}
export XDG_DATA_DIRS=${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-${HOME}/.config}
export XDG_CONFIG_DIRS=${XDG_CONFIG_DIRS:-/etc/xdg}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:-${HOME}/.cache}

# https://www.freedesktop.org/wiki/Software/xdg-user-dirs/
export XDG_DOCUMENTS_DIR=${HOME}/Dokumente
export XDG_DOWNLOAD_DIR=${HOME}/Rumpelkammer
export XDG_MUSIC_DIR=${HOME}/Musik
export XDG_PICTURES_DIR=${HOME}/Fotos
export XDG_VIDEOS_DIR=${HOME}/Videos
