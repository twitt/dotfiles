export AD_COMMON_BUILD_PATH=$HOME/Projekte/ad/common
export AD_INSTALL_PREFIX=$HOME/Projekte/ad/install
export AD_THIRDPARTY_PATH=$HOME/Projekte/ad/thirdparty
export AD_VERBOSE=ON
export AD_RTMAPS_SIGN_PACKAGES=OFF
export AD_IGNORE_SUPPORTED_COMPILER=ON
export HD_CORE_PATH=$HOME/Projekte/hd/core
