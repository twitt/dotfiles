#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

kwriteconfig5 --file gwenviewrc --group "MainWindow" --key "ToolBarsMovable" "Disabled"
kwriteconfig5 --file gwenviewrc --group "SideBar" --key "IsVisible ViewMode" "false"
kwriteconfig5 --file gwenviewrc --group "StatusBar" --key "IsVisible ViewMode" "false"
kwriteconfig5 --file okularrc --group "MainWindow" --key "ToolBarsMovable" "Disabled"
kwriteconfig5 --file okularpartrc --group "Main View" --key "ShowLeftPanel" "false"
kwriteconfig5 --file okularpartrc --group "PageView" --key "MouseMode" "TextSelect"
kwriteconfig5 --file konsolerc --group "Desktop Entry" --key "DefaultProfile" "bash.profile"
kwriteconfig5 --file konsolerc --group "MainWindow" --key "ToolBarsMovable" "Disabled"
kwriteconfig5 --file konsolerc --group "KonsoleWindow" --key "SaveGeometryOnExit" "false"
kwriteconfig5 --file konsolerc --group "KonsoleWindow" --key "ShowMenuBarByDefault" "false"
kwriteconfig5 --file konsolerc --group "SplitView" --key "SplitViewVisibility" "AlwaysHideSplitHeader"
kwriteconfig5 --file yakuakerc --group "Appearance" --key "Skin" "amtare"
kwriteconfig5 --file yakuakerc --group "Appearance" --key "Blur" "true"
kwriteconfig5 --file yakuakerc --group "Appearance" --key "SkinInstalledWithKns" "true"
kwriteconfig5 --file yakuakerc --group "Desktop Entry" --key "DefaultProfile" "zsh.profile"
kwriteconfig5 --file yakuakerc --group "Dialogs" --key "FirstRun" "false"
kwriteconfig5 --file yakuakerc --group "Window" --key "DynamicTabTitles" "true"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "decrease-window-height" "Alt+Shift+Up; Alt+Shift+K"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "decrease-window-width" "Alt+Shift+Left; Alt+Shift+H"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "help_whats_this" "none"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "increase-window-height" "Alt+Shift+Down; Alt+Shift+J"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "increase-window-width" "Alt+Shift+Right; Alt+Shift+L"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "new-session" "Ctrl+Shift+N"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "next-session" "Ctrl+Tab"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "next-terminal" "none"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "previous-session" "Ctrl+Shift+Tab"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "previous-terminal" "none"
kwriteconfig5 --file yakuakerc --group "Shortcuts" --key "view-full-screen" "none"
kwriteconfig5 --file dolphinrc --group "CompactMode" --key "PreviewSize" "22"
kwriteconfig5 --file dolphinrc --group "DetailsMode" --key "PreviewSize" "22"
kwriteconfig5 --file dolphinrc --group "Desktop Entry" --key "DefaultProfile" "zsh.profile"
kwriteconfig5 --file dolphinrc --group "General" --key "PreviewSize" "22"
kwriteconfig5 --file dolphinrc --group "General" --key "BrowseThroughArchives" "true"
kwriteconfig5 --file dolphinrc --group "General" --key "GlobalViewProps" "true"
kwriteconfig5 --file dolphinrc --group "General" --key "ModifiedStartupSettings" "false"
kwriteconfig5 --file dolphinrc --group "General" --key "ShowFullPath" "true"
kwriteconfig5 --file dolphinrc --group "General" --key "ShowSelectionToggle" "false"
kwriteconfig5 --file dolphinrc --group "General" --key "SplitView" "false"
kwriteconfig5 --file dolphinrc --group "InformationPanel" --key "dateFormat" "ShortFormat"
kwriteconfig5 --file dolphinrc --group "MainWindow" --key "MenuBar" "Disabled"
kwriteconfig5 --file dolphinrc --group "MainWindow" --key "ToolBarsMovable" "Disabled"
kwriteconfig5 --file plasma_calendar_holiday_regions --group "General" --key "selectedRegions" "de-by_de,de_de"
kwriteconfig5 --file plasma_calendar_astronomicalevents --group "General" --key "showLunarPhase" "false"
kwriteconfig5 --file plasma_calendar_astronomicalevents --group "General" --key "showSeason" "true"
kwriteconfig5 --file plasmanotifyrc --group "Notifications" --key "PopupPosition" "BottomRight"

gsettings set org.gtk.Settings.FileChooser startup-mode cwd
