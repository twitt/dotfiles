#!/usr/bin/env sh

script_root=$(dirname "$(readlink -f "$0")")

pack() {
  local source="$2"
  [ ! -f "$source" ] && return
  local tmp="$(basename "$source")"
  local target="${tmp%.*}.tgz.gpg"
  cd $script_root
  rm "$target" 2>/dev/null || true
  cp "$source" "$script_root/$tmp"
  echo "pack $source to $target"
  tar -cz "$tmp" | gpg --batch --passphrase "$1" -c -o "$target"
  rm "$script_root/$tmp"
}

unpack() {
  local target="$2"
  [ -f "$target" ] && echo "$target does already exist.. skip" && return
  local tmp="$(basename "$2")"
  local source_enc="$script_root/${tmp%.*}.tgz.gpg"
  cd $script_root
  [ ! -f "$source_enc" ] && echo "$source_enc does not exist" && exit 1
  echo "unpack $source_enc to $target"
  gpg -d --batch --passphrase "$1" "$source_enc" | tar xz
  mkdir -p "$(dirname "$target")"
  mv "$script_root/$tmp" "$target"
}

if [ -z "$1" ]; then
  echo "call it using './share.sh \"\$(chezmoi execute-template \"{{ .gui.share_pass }}\")\"'" && exit 1
elif [ -z "$2" ]; then
  pack "$1" "$HOME/.local/share/fonts/normal.otf"
  pack "$1" "$HOME/.local/share/fonts/fixed.ttf"
  pack "$1" "$HOME/.local/share/wallpapers/background.jpg"
else
  unpack "$1" "$2"
fi
