#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

# Theme
# kwriteconfig5 --file kwinrc --group "TabBox" --key "DesktopLayout" "org.kde.breeze.desktop"
# kwriteconfig5 --file kwinrc --group "TabBox" --key "DesktopListLayout" "org.kde.breeze.desktop"
# kwriteconfig5 --file kwinrc --group "TabBox" --key "LayoutName" "org.kde.breeze.desktop"
# kwriteconfig5 --file kwinrc --group "org.kde.kdecoration2" --key "library" "org.kde.breeze"
# kwriteconfig5 --file kwinrc --group "org.kde.kdecoration2" --key "theme" "Breeze"
kwriteconfig5 --file kwinrc --group "org.kde.kdecoration2" --key "ButtonsOnLeft" "XFS"
kwriteconfig5 --file kwinrc --group "org.kde.kdecoration2" --key "ButtonsOnRight" "HIA"
kwriteconfig5 --file kwinrc --group "org.kde.kdecoration2" --key "ShowToolTips" "false"

# Input
kwriteconfig5 --file kwinrc --group "MouseBindings" --key "CommandAllKey" "Meta"
kwriteconfig5 --file kwinrc --group "MouseBindings" --key "CommandAllWheel" "Maximize/Restore"
kwriteconfig5 --file kwinrc --group "ElectricBorders" --key "TopLeft" "None"
kwriteconfig5 --file kwinrc --group "TabBox" --key "BorderActivate" "9"
kwriteconfig5 --file kwinrc --group "TabBox" --key "DesktopMode" "0"
kwriteconfig5 --file kwinrc --group "Effect-PresentWindows" --key "BorderActivateAll" "9"
kwriteconfig5 --file kwinrc --group "Windows" --key "BorderSnapZone" "20"
kwriteconfig5 --file kwinrc --group "Windows" --key "WindowSnapZone" "20"
kwriteconfig5 --file kwinrc --group "Windows" --key "ElectricBorderCooldown" "500"
kwriteconfig5 --file kwinrc --group "Windows" --key "ElectricBorderDelay" "400"
kwriteconfig5 --file kwinrc --group "Windows" --key "ElectricBorders" "1"

# Nightcolor
kwriteconfig5 --file kwinrc --group "NightColor" --key "Active" "true"
kwriteconfig5 --file kwinrc --group "NightColor" --key "LatitudeFixed" "40.09"
kwriteconfig5 --file kwinrc --group "NightColor" --key "LongitudeFixed" "11.44"
kwriteconfig5 --file kwinrc --group "NightColor" --key "Mode" "Location"
kwriteconfig5 --file kwinrc --group "NightColor" --key "NightTemperature" "4200"

# mkdir -p ~/.local/share/kwin/scripts/
# kwriteconfig5 --file kwinrc --group "Plugins" --key "forceblurEnabled" "true"

dbus-send --dest=org.kde.KWin /KWin org.kde.KWin.reloadConfig

if [[ -f "$home/.config/gtk-3.0/settings.ini" ]]; then
  sed -i 's/^gtk-decoration-layout=.*/gtk-decoration-layout=close:minimize,maximize/' "$HOME/.config/gtk-3.0/settings.ini"
fi
if [[ -f "$home/.config/xsettingsd/xsettingsd.conf" ]]; then
  sed -i 's/^Gtk\/DecorationLayout .*/Gtk\/DecorationLayout \"close:minimize,maximize\"/' "$HOME/.config/xsettingsd/xsettingsd.conf"
fi
