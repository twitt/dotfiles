[Appearance]
AntiAliasFonts=true
BoldIntense=false
ColorScheme=amtare
Font=FiraCode Nerd Font,14,-1,5,50,0,0,0,0,0

[Cursor Options]
CursorShape=2
UseCustomCursorColor=false

[General]
Command=/bin/zsh
Environment=TERM=konsole-256color,COLORTERM=truecolor,TERMFONT=nerdfont
Icon=akonadiconsole
Name=zsh
Parent=FALLBACK/
TerminalColumns=120
TerminalMargin=0

[Interaction Options]
AutoCopySelectedText=false
CopyTextAsHTML=false
CtrlRequiredForDrag=true
MiddleClickPasteMode=1
MouseWheelZoomEnabled=true
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true

[Keyboard]
KeyBindings=amtare

[Scrolling]
HistoryMode=2
HistorySize=100000
ScrollBarPosition=1

[Terminal Features]
BidiRenderingEnabled=false
BlinkingCursorEnabled=false
BlinkingTextEnabled=true
FlowControlEnabled=true
