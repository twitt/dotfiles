import QtQuick 2.5
import QtQuick.Window 2.2

Image {
    id: root
    source: "images/background.png"
    fillMode: Image.PreserveAspectCrop

    property int stage

    onStageChanged: {
        if (stage == 1) {
            introAnimation.running = true
        }
    }
    Image {
        id: topRect
        anchors.horizontalCenter: parent.horizontalCenter
        y: root.height
        source: "images/rectangle.png"
        TextMetrics {
            id: units
            text: "M"
            property int gridUnit: boundingRect.height
            property int largeSpacing: units.gridUnit
            property int smallSpacing: Math.max(2, gridUnit/4)
        }
        Image {
            source: "images/arch.png"
            anchors.centerIn: parent
        }

        Text {
            id: date
            text:Qt.formatDateTime(new Date(),"ddd d.M | HH:mm")
            font.pointSize: 24
            color: "#f9f9f9"
            opacity: 1.00
            font { family: "SF Compact Display"; weight: Font.Light ;capitalization: Font.Capitalize}
            anchors {
                top: parent.bottom
                topMargin: 310
                horizontalCenter: parent.horizontalCenter
            }
        }

        Rectangle {
            radius: 1
            color: "#858585"
            anchors {
                top: parent.bottom
                topMargin: 356
                horizontalCenter: parent.horizontalCenter
            }
            height: 3
            width: 128
            Rectangle {
                radius: 1
                anchors {
                    left: parent.left
                    top: parent.top
                    bottom: parent.bottom
                }
                width: (parent.width / 6) * (stage - 1)
                color: "#f9f9f9"
                Behavior on width {
                    PropertyAnimation {
                        duration: 250
                        easing.type: Easing.InOutQuad
                    }
                }
            }
        }
    }

    SequentialAnimation {
        id: introAnimation
        running: false

        ParallelAnimation {
            PropertyAnimation {
                property: "y"
                target: topRect
                to: root.height / 3
                duration: 1000
                easing.type: Easing.InOutBack
                easing.overshoot: 1.0
            }

            PropertyAnimation {
                property: "y"
                target: bottomRect
                to: 2 * (root.height / 3) - bottomRect.height
                duration: 1000
                easing.type: Easing.InOutBack
                easing.overshoot: 1.0
            }
        }
    }
}
