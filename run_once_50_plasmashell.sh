#!/usr/bin/env bash
# vim: set ft=sh:
which plasmashell >/dev/null 2>/dev/null || exit 0

kwriteconfig5 --file "kactivitymanagerd-pluginsrc" --group "Plugin-org.kde.ActivityManager.Resources.Scoring" --key "what-to-remember" "1"

kwriteconfig5 --file plasma-localerc --group "Formats" --key "LANG" "de_DE.UTF-8"
kwriteconfig5 --file plasma-localerc --group "Translations" --key "LANGUAGE" "de:en_US"
kwriteconfig5 --file baloofilerc --group "Basic Settings" --key "Indexing-Enabled" "false"
kwriteconfig5 --file ksmserverrc --group "General" --key "loginMode" "emptySession"
kwriteconfig5 --file klipperrc --group "Actions" --key "EnableMagicMimeActions" "false"
kwriteconfig5 --file klipperrc --group "General" --key "MaxClipItems" "50"
kwriteconfig5 --file klipperrc --group "General" --key "PreventEmptyClipboard" "true"
kwriteconfig5 --file klipperrc --group "General" --key "SelectionTextOnly" "false"
kwriteconfig5 --file klipperrc --group "General" --key "IgnoreSelection" "true"

if [ ! -f "$HOME/.local/share/desktop/.directory" ]; then
  mkdir -p "$HOME/.local/share/desktop"
  cat >"$HOME/.local/share/desktop/.directory" <<EOF
[Desktop Entry]
Icon=user-desktop
Type=Directory
EOF
fi

if [ -f "$HOME/.config/user-dirs.dirs" ]; then
  rm "$HOME/.config/user-dirs.dirs"
fi
cat >"$HOME/.config/user-dirs.dirs" <<EOF
XDG_DESKTOP_DIR="\$HOME/.local/share/desktop"
XDG_DOCUMENTS_DIR="\$HOME/Dokumente"
XDG_DOWNLOAD_DIR="\$HOME/Rumpelkammer"
XDG_MUSIC_DIR="\$HOME/Musik"
XDG_PICTURES_DIR="\$HOME/Fotos"
XDG_VIDEOS_DIR="\$HOME/Videos"
EOF
