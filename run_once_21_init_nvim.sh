#!/bin/sh
which nvim >/dev/null 2>/dev/null || exit 0

get_vim_spell() {
  lang_url="http://ftp.vim.org/pub/vim/runtime/spell/${1}"
  lang_path="$HOME/.config/nvim/spell/${1}"
  test ! -f "${lang_path}.spl" && curl -fLo "${lang_path}.spl" "${lang_url}.spl"
  test ! -f "${lang_path}.sug" && curl -fLo "${lang_path}.sug" "${lang_url}.sug"
  true
}

plug_path="$HOME/.local/share/nvim/site/autoload/plug.vim"
if [ ! -f "$plug_path" ]; then
  mkdir -p "$HOME/.config/nvim/spell"
  mkdir -p "$(dirname "$plug_path")"
  curl -fLo "$plug_path" https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  nvim -es -u "$HOME/.config/nvim/init.vim" -i NONE -c "PlugInstall" -c "qa"
  nvim -i NONE -c "CocInstall coc-sh coc-python coc-json" -c "qa"
else
  nvim -c "PlugUpdate" -c "qa"
fi

# get new language files
get_vim_spell "en.utf-8"
get_vim_spell "de.utf-8"

# compile add files if not already done
for lang_add in "$HOME/.config/nvim/spell"/*add; do
  [ ! -f "${lang_add}.spl" ] && nvim -i NONE -c "mkspell $lang_add" -c "qa"
done
true
